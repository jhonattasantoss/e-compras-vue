import LocalStorage from '../common/LocalStorage'
import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)
require('../common/Interceptor')

export default {

    get token(){
        return LocalStorage.get('token')
    },
    set token(value){
        return value ? LocalStorage.set('token', value) : LocalStorage.remove('token')
    },
    accessToken(login,password){

        let url = 'http://simulado.cotacoesecompras.com.br/api/v2/auth/login';
        return Vue.http.post(url, {
            login: login,
            password: password
        }).then((response) => {
          this.token = response.data.token;
        })
    },
    getAthenticatedUsuario(){
      let url = 'http://simulado.cotacoesecompras.com.br/api/v2/auth/validate'
      return Vue.http.get(url).then((response) => {
        return response;
      })
    },
    getAuthorizationHeader(){
        return `Bearer ${this.token}`;
    }

}
