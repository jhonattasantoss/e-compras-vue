export default {
    set(key, value){
        window.sessionStorage[key] = value;
        return this.get(key);
    },
    get(key, defaultValue){
        return window.sessionStorage[key] || defaultValue;
    },
    setObject(key, value){
        window.sessionStorage[key] = JSON.stringify(value);
        return this.getObject(key);
    },
    getObject(key){
        return JSON.parse(this.get(key) || null);
    },
    remove(key){
        window.sessionStorage.removeItem(key);
    }
}
