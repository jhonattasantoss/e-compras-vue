import Vue from 'vue'
import JWTToken from '../auth/JWTToken'

Vue.http.interceptors.push((request, next) => {
    request.headers.set('Authorization', JWTToken.getAuthorizationHeader());
    next();
});
