import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)
require('./Interceptor')

const User = Vue.resource('http://simulado.cotacoesecompras.com.br/api/v2/auth/validate');

export  {User};
