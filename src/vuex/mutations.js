import LocalStorage from '@/services/common/LocalStorage'
import JWTToken from '@/services/auth/JWTToken'

export default {
  'SET_USER'(state, payload){
      LocalStorage.setObject('user', payload)
      state.auth.user = payload;
  },
  'AUTHENTICATED'(state, payload){
      state.auth.check = true
  },
  'UNAUTHENTICATED'(state, payload){
      state.auth.check = false
      state.auth.user = null
      LocalStorage.remove('user')
      JWTToken.token = null
  }
}
