import JWTToken from '@/services/auth/JWTToken'
import {User} from '@/services/common/Resources'

export default {
  login(context, payload){
     return JWTToken.accessToken(payload.login, payload.password)
          .then((response) => {
              context.commit('AUTHENTICATED');
              //Se carregar o tokem chama a ação pra salvar os dados do usuario no state e no storage
              context.dispatch('getUser');
              //JWTToken.getAthenticatedUsuario();
          })
  },
  logout(context, payload){
      context.commit('UNAUTHENTICATED');
  },
  getUser(context,payload){
      JWTToken.getAthenticatedUsuario()
      .then((response) => {
        context.commit('SET_USER', response.body)
      })
  }
}
