import JWTToken from '@/services/auth/JWTToken'
import LocalStorage from '@/services/common/LocalStorage'

export default {
  auth: {
    user : LocalStorage.getObject('user'),
    check: JWTToken.token != null
  }
}
