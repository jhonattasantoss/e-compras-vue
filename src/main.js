// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './vuex/store'

import SessionStorage from './services/common/SessionStorage'
import LocalStorage from './services/common/LocalStorage'

//require('../node_modules/bootstrap/dist/css/bootstrap.min.css')
require('../node_modules/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')
require('./assets/vendors/bower_components/animate.css/animate.min.css')
require('./assets/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css')
require('./assets/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')
require('./assets/css/app.min.1.css')
require('./assets/css/app.min.2.css')

require('./assets/vendors/bower_components/jquery/dist/jquery.min.js')
require('./assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')
require('./assets/vendors/bower_components/moment/min/moment.min.js')
require('./assets/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js')
require('./assets/vendors/bower_components/Waves/dist/waves.min.js')
require('./assets/vendors/bootstrap-growl/bootstrap-growl.min.js')
require('./assets/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js')
//require('./assets/vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')
require('./assets/js/functions.js')
require('./assets/js/demo.js')

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {

  if(to.meta.auth && !LocalStorage.get('token')){
    SessionStorage.set('linkTo', to.name)
    return router.push({name: 'Login'})
  }

  if(!to.meta.auth && LocalStorage.get('token')){
    return router.push({name: 'Home'})
  }


  next()
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
