import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home.Component'
import Login from '@/components/auth/Login.Component'
import Logout from '@/components/auth/Logout.Component'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        auth: true
      }
    },
    {
      path: '/auth/login',
      name: 'Login',
      component: Login,
      meta: {
        auth: false
      }
    },
    {
        path: '/auth/logout',
        name: 'Logout',
        component: Logout,
        meta:{ auth: true}
    }
  ]
})
